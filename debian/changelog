waffle (1.7.0-1) UNRELEASED; urgency=medium

  * New upstream release
  * d/patches: Rebase on v1.7.0
  * d/patches: Drop 5 patches which were upstreamed in new version
  * d/rules: Fix xenabledenabled_egl -> x11_egl typo
  * d/control: Require cmake version >= 2.8.12
  * d/control: Add wayland-protocols build dependency

 -- Adrian Ratiu <adrian.ratiu@collabora.com>  Thu, 25 Mar 2021 12:04:40 +0000

waffle (1.6.3-1) unstable; urgency=medium

  * New upstream release
  * debian: Convert from cmake to meson build
  * d/patches: Rebase on v1.6.3
  * d/patches: Cherry-pick patch from upstream to produce cmake files
  * d/waffle-utils.install: Upstream now installs bash-completion properly
  * d/patches: Add patch to hide symbols of threads helper lib

 -- Jordan Justen <jljusten@debian.org>  Thu, 25 Feb 2021 00:51:00 -0800

waffle (1.6.1-3) unstable; urgency=medium

  * d/rules: Override dh_missing for arch-indep builds. Closes: #972970

 -- Jordan Justen <jljusten@debian.org>  Mon, 02 Nov 2020 01:51:32 -0800

waffle (1.6.1-2) unstable; urgency=medium

  * debian/control: Update Jordan's email address
  * debian/not-installed: Add for debhelper-compat 13
  * debian/control: Update to debhelper-compat 13

 -- Jordan Justen <jljusten@debian.org>  Mon, 26 Oct 2020 03:31:30 -0700

waffle (1.6.1-1) unstable; urgency=medium

  * New upstream release
  * d/upstream/signing-key.asc: Add Emil Velikov's gpg key for 1.6.1 release
  * d/control: Add bash-completion build dependency
  * d/waffle-utils.install: Install bash-completions
  * d/patches: Fix some spelling errors caught by lintian
  * d/rules: Fix lintian hardening-no-bindnow
  * d/libwaffle-1-0.symbols: Add symbol file for lintian no-symbols-control-file
  * d/control: Split out Pre-Depends for libwaffle-1-0
  * d/control: Fix extended-description-is-probably-too-short for libwaffle-1-0
  * d/patches: Patch docbookx.dtd url to local file path
  * d/control: Depend on docbook-xml for 4.2/docbookx.dtd
  * d/patches: Add refpurpose on waffle_enum page for lintian manpage-has-bad-whatis-entry

 -- Jordan Justen <jordan.l.justen@intel.com>  Sun, 12 Apr 2020 22:54:23 -0700

waffle (1.6.0-4) unstable; urgency=medium

  * d/control: Drop binutils from Build-Depends. Closes: #877605
  * d/control: Update Standards-Version to 4.5.0
  * d/control: Use debhelper-compat (= 12)

 -- Jordan Justen <jordan.l.justen@intel.com>  Thu, 12 Mar 2020 15:13:57 -0700

waffle (1.6.0-3) unstable; urgency=medium

  * d/control: build-dep libx11-dev => libx11-xcb-dev (Closes #948779)
  * d/gbp.conf: Add git-buildpackage config file

 -- Jordan Justen <jordan.l.justen@intel.com>  Thu, 23 Jan 2020 16:59:58 -0800

waffle (1.6.0-2) unstable; urgency=medium

  * Start new changelog
  * d/rules: Enable surfaceless_egl
  * debian: Increase debhelper to 12
  * d/control: Increase Standards-Version to 4.4.0

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 19 Jul 2019 01:50:12 -0700

waffle (1.6.0-1) experimental; urgency=medium

  * New upstream release.
    - wflinfo can output JSON [Dylan Baker]
    - EGL/GBM: Add support for modifiers [Jason Ekstrand]
  * d/control: Move Vcs-* to salsa.debian.org/xorg-team
  * d/watch: Update watch file for upstream freedesktop.org move
  * d/control: Update Standards-Version to 4.3.0
  * d/copyright: Use debian doc https url
  * d/compat: Update from 9 to 11
  * d/compat: Update debhelper from 9 to 11
  * debian: Add pgp verification with Chad Versace's key
  * debian: Add Dylan Baker's gpg key for 1.6.0 release
  * d/watch: Update version to 4

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 07 Jun 2019 14:43:49 -0700

waffle (1.5.2-4) unstable; urgency=low

  * Add 'Multi-arch: same' for libwaffle-1-0 and libwaffle-dev
  * Move Standards-Version to 4.1.1
  * Change to https url to fix lintian vcs-field-uses-insecure-uri

 -- Jordan Justen <jordan.l.justen@intel.com>  Wed, 18 Oct 2017 17:07:59 -0700

waffle (1.5.2-3) unstable; urgency=low

  * Add watch file (Closes: #786994).

 -- Jordan Justen <jordan.l.justen@intel.com>  Sat, 11 Feb 2017 00:58:03 -0800

waffle (1.5.2-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Add B-D: libudev-dev (Closes: #846704).

 -- Andrey Rahmatullin <wrar@debian.org>  Wed, 14 Dec 2016 22:53:42 +0500

waffle (1.5.2-2) unstable; urgency=low

  * Improve debian package descriptions

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 08 Sep 2015 23:50:31 -0700

waffle (1.5.2-1) unstable; urgency=low

  * New upstream release

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 25 Aug 2015 14:52:18 -0700

waffle (1.5.1-1) unstable; urgency=low

  * New upstream release
  * Add WaffleConfig*.cmake to libwaffle-dev package

 -- Jordan Justen <jordan.l.justen@intel.com>  Thu, 22 Jan 2015 12:51:49 -0800

waffle (1.5.0-1) unstable; urgency=low

  * New upstream release

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 16 Dec 2014 11:06:19 -0800

waffle (1.5.0~rc1-1) unstable; urgency=low

  * New upstream release candidate

 -- Jordan Justen <jordan.l.justen@intel.com>  Thu, 04 Dec 2014 23:18:00 -0800

waffle (1.4.3-1) unstable; urgency=low

  * New upstream release

 -- Jordan Justen <jordan.l.justen@intel.com>  Tue, 09 Dec 2014 16:04:52 -0800

waffle (1.4.2-1) unstable; urgency=low

  * New upstream release

 -- Jordan Justen <jordan.l.justen@intel.com>  Fri, 05 Dec 2014 00:02:35 -0800

waffle (1.4.1-1) unstable; urgency=low

  * New upstream release

 -- Jordan Justen <jordan.l.justen@intel.com>  Sat, 08 Nov 2014 12:13:54 -0800

waffle (1.4.0-1) unstable; urgency=low

  * New upstream release

 -- Jordan Justen <jordan.l.justen@intel.com>  Mon, 22 Sep 2014 11:16:49 -0700

waffle (1.4.0~rc1-1) unstable; urgency=low

  * New upstream release candidate

 -- Jordan Justen <jordan.l.justen@intel.com>  Mon, 08 Sep 2014 08:18:01 -0700

waffle (1.3.90-1) unstable; urgency=low

  * Use "non-native" 1.3.90-1 debian version number
  * Added missing include files in dev package. (closes: #748300)

 -- Jordan Justen <jordan.l.justen@intel.com>  Thu, 15 May 2014 16:51:10 -0700

waffle (1.3.90) unstable; urgency=low

  * Bump version to match waffle's current 1.3.90 version number

 -- Jordan Justen <jordan.l.justen@intel.com>  Sun, 13 Apr 2014 13:20:35 -0700

waffle (1.3.0) unstable; urgency=low

  * Add debian packaging

 -- Jordan Justen <jordan.l.justen@intel.com>  Mon, 30 Dec 2013 14:50:51 -0800
